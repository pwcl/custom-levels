let levelList;

async function getLevels() {
    let list = await fetch("levels.txt");
    let levels = await list.text();
    levelList = levels.split("\n");
    
    for (let i = 0; i < levelList.length; i++) {
        if (levelList[i] != "") {
			postMessage(`<button class="level" onclick="bundleLevel('${i}');"><h3>${levelList[i]}</h3>\n<img src="levels/${levelList[i]}/poster.png" class="poster"/></button>`);
		}
    }
}

onmessage = function(e) {
    switch (e.data) {
        case "setup":
            getLevels();
            break;
    }
}
  